window.unicoisaConfig = { 
  walletName: 'Bitreals',
  mainColor: '',
  secondaryColor: '',
  assets: [
    {
      assetId: 'LFu6pNp5FLHQu1RERkYEjPjxFZLD3zNJAbhYz',
      name: 'Bitreal',
      symbol: 'R$',
      pluralSymbol: 'R$'
    }
  ],
  defaultAsset: 'LFu6pNp5FLHQu1RERkYEjPjxFZLD3zNJAbhYz',
  logo: '',
  coluApiKey: '',
  allowAssetChange: true,
  noUserColors: false,
  needsBackup: true
};